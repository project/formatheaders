====================
Readme
====================
This module reformats the titles of blocks and panel panes with user-supplied
code.  It lets you supply different formats for uppercase and lowercase text.
You can select whether you want to apply formatting to block titles, panel pane
titles, or both.

Formatting ability is limited only by your knowledge of CSS.  Add color, text
decoration, webfonts, whatever you want!

====================
Usage
====================
1. Install and enable the module as usual
2. Go to the configure page: admin/config/content/headers
3. Write custom styling code for uppercase and/or lowercase (using CSS format)
4. Select whether you want the styling to appear on block titles, panel pane
    titles, or both

====================
API
====================
This module has an open API!  Use format_headers_regex($string) to process any
string using this module.

====================
Credits
====================
This module was created by KoplowiczandSons.com.
